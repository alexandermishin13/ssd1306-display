#include <ssd1306.h>
#include <unistd.h>

/* gpio bit banging managed */
// For libgpio it is GPIO numbers not PIN's
// e.g. for RPI2 GPIO 20 is PIN 38, GPIO 21 is PIN 40.
// That example for Orange Pi Zero
#define CLK 11
#define DIO 12
//SSD1306_128X32 ssd1306(CLK, DIO, "/dev/gpioc0"); // clock, data pins, gpio controller device
//SSD1306_128X64 ssd1306(CLK, DIO); // clock, data pins, gpio controller device

/* i2c managed */
//SSD1306_128X64 ssd1306("/dev/iic2"); // I2C device name
SSD1306_128X64 ssd1306; // Omit the parentheses if You use only default arguments

int
main(int argc, char **argv)
{
  ssd1306.Setup();
  ssd1306.displayOn();
  ssd1306.display(); //Adafruit logo is visible until use clearDisplay.
  sleep(5);

  /* Clear the display */
  ssd1306.clearDisplay();

  char text[] = "This is a SSD1306\ngpio driver demo\nfor <Fruit> Pi";
  ssd1306.drawString(text);
  ssd1306.display();
  sleep(5);

  /* Clear the display */
  ssd1306.clearDisplay();

  ssd1306.dim(true);
  ssd1306.startScrollRight(00,0xFF);
  sleep(5);
  ssd1306.stopScroll();
  ssd1306.dim(false);

  /* Draw rectangles */
  ssd1306.fillRect(10,10, 50, 20, WHITE);
  ssd1306.drawRect(80, 10, 130, 50, WHITE);
  ssd1306.display();
  sleep(5);

  /* Clear the display */
  ssd1306.clearDisplay();

  /* Set text size */
  char text2[] = "END";
  ssd1306.setTextSize(2);
  ssd1306.drawString(text2);
  ssd1306.display();
  sleep(5);

  /* Turn off the display */
  ssd1306.displayOff();
}
