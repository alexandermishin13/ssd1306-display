# ssd1306-display

## About

The library was written mostly for the ARM platform running FreeBSD, which often
has problems implementing the I2C protocol.
Such a platform until recently was OrangePi Zero - `no SPI`, `no I2C`, but `GPIO only`.
After `I2C` is earned I added an appropriate code to the library.
You can switch them by flag at compile time.
On the other hand, `I2C` is the standard ability to connect multiple devices.

## Compilation and installation

You must decide what a method You will to use for manage the Your display on
Your platform, `GPIO` or `I2C`.
In my opinion `GPIO` have a bit faster reaction time and of course You free
to choose the pins by yourself.

If You choose a `GPIO Bit Banging` method instead of an `I2C` one, You can uncomment a `SSD1306_GPIO` flag into `ssd1306.h`
and compile the library. Otherwise leave the flag commented. (See `./demo.cpp` for a difference of display initializations).

To compile and install the library use:
```
make
sudo make install
```
To uninstall library try:
```
sudo make uninstall
```
To make a demo program use:
```
make demo
./demo
```

## Status

Stable.

Models:
* SSD1306 128x32 (tested)
* SSD1306 128x64 (tested)
* SSD1306 96x16 (untested)

Features:
* Adafruit logo
* Text strings
* Font size change
* Lines
* Rectangles

## Bug

* After changing the display control method from `gpiobb` to `i2c`,
the display may not respond to commands.
To restore the display, remove and reapply its power.
