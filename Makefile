CFLAGS=-fpic -c -Wall -O2
PREFIX=/usr/local
MAJOR=1

all: libssd1306.so

ssd1306.o: ssd1306.cpp ssd1306.h
	$(CXX) $(CFLAGS) -o ssd1306.o ssd1306.cpp

libssd1306.so: ssd1306.o
	$(CXX) --shared -fpic -Wl,-soname,libssd1306.so -o libssd1306.so.${MAJOR} ssd1306.o

clean:
	rm *.o *.so* demo

demo.o: demo.cpp
	$(CXX) -c -Wall -O2 -I/usr/local/include demo.cpp

demo: demo.o
	$(CXX) demo.o -lssd1306 -lgpio -lc -L/usr/local/lib -L./ -o demo

install: libssd1306.so
	sudo install -m 0644 -o root -g wheel ssd1306.h ${PREFIX}/include/
	sudo install -m 0755 -o root -g wheel -s libssd1306.* ${PREFIX}/lib/
	sudo install -lr ${PREFIX}/lib/libssd1306.so.${MAJOR} ${PREFIX}/lib/libssd1306.so

uninstall:
	sudo rm ${PREFIX}/include/ssd1306.h
	sudo rm ${PREFIX}/lib/libssd1306.so*
