//  Author:Alexander Mishin
//  Date:10 Jun,2019
//
/*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "ssd1306.h"
#include "oled_fonts.h"

#define pgm_read_byte(addr) (*(const unsigned char *)(addr))
#define swap_values(a, b) { int t = a; a = b; b = t; }

#define rotation 0

/*
 * Constructor for SSD1306
 */

#ifdef SSD1306_GPIO

SSD1306::SSD1306(
    gpio_pin_t pinClk,
    gpio_pin_t pinData,
    const char* gpiocDevice,
    uint8_t width,
    uint8_t height,
    uint8_t slaveAddress,
    uint8_t vccState
) : width(width),
    height(height),
    gpio_handle(gpio_open_device(gpiocDevice)),
    pin_clk(pinClk),
    pin_data(pinData)
{

  if (gpio_handle == GPIO_INVALID_HANDLE)
  {
    perror(gpiocDevice);
    exit(EXIT_FAILURE);
  };

  char name_scl[] = "ssd1306_scl";
  char name_sda[] = "ssd1306_sda";
  gpio_pin_pullup(gpio_handle, pin_clk);
  gpio_pin_output(gpio_handle, pin_clk);
  gpio_pin_set_name(gpio_handle, pin_clk, name_scl);
  gpio_pin_pullup(gpio_handle, pin_data);
  gpio_pin_output(gpio_handle, pin_data);
  gpio_pin_set_name(gpio_handle, pin_data, name_sda);

#else

SSD1306::SSD1306(
    const char* iicDevice,
    uint8_t width,
    uint8_t height,
    uint8_t slaveAddress,
    uint8_t vccState
) : width(width),
    height(height),
    iic_handle(open(iicDevice, O_RDWR))
{

  if ( iic_handle < 0)  {
    perror(iicDevice);
    exit(EXIT_FAILURE);
  }

#endif

  text_size = 1;
  wrap = 1;
  vccstate = vccState;

  cursor_y = 0;
  cursor_x = 0;

  page_start = 0x00;
  page_end = height / 8 - 1;

  buffer_size = width * height / 8;
  buffer1 = new Buffer(buffer_size);

  //data_buffer = new uint8_t[buffer_size + 1];
  //data_buffer[0] = SSD1306_DATA_REG;
  //buffer = &data_buffer[1];

  drawLogo();

  slave_addr = slaveAddress << 1; // Read only
};

/*
 * Destructor for SSD1306
 */
SSD1306::~SSD1306()
{
  /* Clear ant light display off */
  displayOff();

  /* Destroy the data buffer */
  delete buffer1;
  //delete [] data_buffer;
  //data_buffer = NULL;

  //gpio_close(gpio_handle);
}

#ifdef SSD1306_GPIO

/*
 * Transmit a signal to the display to start a transmition a byte
 */
void
SSD1306::send_start(void)
const
{
  gpio_pin_high(gpio_handle, pin_data); 
  gpio_pin_high(gpio_handle, pin_clk);
  gpio_pin_low(gpio_handle, pin_data); 
  gpio_pin_low(gpio_handle, pin_clk); 
};


/*
 * Transmit a byte to the display
 */
void
SSD1306::send_byte(uint8_t byte)
const
{
  uint8_t b = byte;

  /* Sent 8bit data */
  for(uint8_t i = 0; i < 8; i++)
  {
    /* Set next data bit */
    gpio_pin_set(gpio_handle, pin_data, (gpio_value_t)((b & 0x80) > 0));
    // A rising edge of CLK
    gpio_pin_high(gpio_handle, pin_clk);
    b <<= 1;
    // A falling edge of CLK
    gpio_pin_low(gpio_handle, pin_clk);
  }

  /* Read for a low level of data on 9th clock sync */
  gpio_pin_input(gpio_handle, pin_data);
  gpio_pin_high(gpio_handle, pin_clk);
  while(gpio_pin_get(gpio_handle, pin_data))
  {
    // Waiting for zero data bit as an ACK
  }
  gpio_pin_output(gpio_handle, pin_data);
  gpio_pin_low(gpio_handle, pin_clk);
};


/*
 * Transmit a signal to the display to stop a transmition a byte
 */
void
SSD1306::send_stop(void)
const
{
  gpio_pin_low(gpio_handle, pin_clk);
  gpio_pin_low(gpio_handle, pin_data);
  gpio_pin_high(gpio_handle, pin_clk);
  gpio_pin_high(gpio_handle, pin_data); 
};

#else

char
SSD1306::iic_write(int iic_handle, uint8_t data[], uint16_t size)
const
{
  struct iic_msg msg[1] =
  {
    {slave_addr, IIC_M_WR, size, data}
  };
  struct iic_rdwr_data rdwr = {msg, 1};

  if ( ioctl(iic_handle, I2CRDWR, &rdwr) < 0 )  {
    perror("I2CRDWR");

    return(-1);
  }

  return(0);
}

#endif

/* Send one byte command */
void
SSD1306::send_command(uint8_t command)
const
{

#ifdef SSD1306_GPIO

  send_start();
  send_byte(slave_addr);
  send_byte(SSD1306_CONTROL_REG);
  send_byte(command);
  send_stop();

#else

  uint8_t buf[2] = {SSD1306_CONTROL_REG, command};
  iic_write(iic_handle, buf, 2);

#endif

};

/* Send command and parameter */
void
SSD1306::send_command(uint8_t command, uint8_t param)
const
{

#ifdef SSD1306_GPIO

  send_start();
  send_byte(slave_addr);
  send_byte(SSD1306_CONTROL_REG);
  send_byte(command);
  send_byte(param);
  send_stop();

#else

  uint8_t buf[3] = {SSD1306_CONTROL_REG, command, param};
  iic_write(iic_handle, buf, 3);

#endif

};

/* Send command with two parameters */
void
SSD1306::send_command(uint8_t command, uint8_t param1, uint8_t param2)
const
{

#ifdef SSD1306_GPIO

  send_start();
  send_byte(slave_addr);
  send_byte(SSD1306_CONTROL_REG);
  send_byte(command);
  send_byte(param1);
  send_byte(param2);
  send_stop();

#else

  uint8_t buf[4] = {SSD1306_CONTROL_REG, command, param1, param2};
  iic_write(iic_handle, buf, 4);

#endif

};

/* Send all the buffer of data */
void
SSD1306::send_data_buffer(void)
const
{

#ifdef SSD1306_GPIO

  send_start();
  send_byte(slave_addr);

  uint16_t i;
  for (i = 0; i < buffer1->buffer_size; i++) {
    send_byte(buffer1->buffer[i]);
  };

  send_stop();

#else

  iic_write(iic_handle, buffer1->buffer, buffer1->buffer_size);

#endif

};

void
SSD1306::Setup(void)
const
{
  // init SSD1306 settings
  send_command(SSD1306_DISPLAYOFF);  // Display off

  send_command(SSD1306_SETDISPLAYCLOCKDIV, 0x80);
  /* Set Multiplex Ratio */
  send_command(SSD1306_SETMULTIPLEX, height-1);
  /* Set Display Offset (no offset) */
  send_command(SSD1306_SETDISPLAYOFFSET, 0x00);

  send_command(SSD1306_SETSTARTLINE | 0x0);  // Set display Start Line
  send_command(SSD1306_SEGREMAP | 0x1);  // Set Segment Re-Map
  send_command(SSD1306_COMSCANDEC);  // Set COM Output Scan Direction

  /* Set COM Pins Hardware Configuration */
  send_command(SSD1306_SETCOMPINS, com_pins);

  send_command(SSD1306_DISPLAYALLON_RESUME);  // Set Entire Display On/Off

  send_command(SSD1306_NORMALDISPLAY);  // Set Normal/Inverse Display

  /* Set VCOMH Deselect Level */
  send_command(SSD1306_SETVCOMDETECT, 0x40);

  /* Set Contrast Control */
  send_command(SSD1306_SETCONTRAST, contrast);

  send_command(SSD1306_SETPRECHARGE, precharge);
  send_command(SSD1306_CHARGEPUMP, charge_pump);

  send_command(0x00);  // Set Lower Column Start Address

  send_command(0x10);  // Set Higher Column Start Address

  send_command(0xB0);  // Set Page Start Address for Page Addressing Mode

  /* Set Memory Addressing Mode
   * 00 - Horizontal Addressing Mode
   * 01 - Vertical Addressing Mode
   * 02 - Page Addressing Mode
   */
  send_command(SSD1306_MEMORYMODE, 0x00);
  /* Column start (0 = reset) and stop (127 = reset) addresses
   * (only for horizontal or vertical mode)
   */
  send_command(SSD1306_COLUMNADDR, 0, width-1);
  /* Set Page start and stop addresses */
  send_command(SSD1306_PAGEADDR, page_start, page_end);
  /* Deactivate Scroll */
  send_command(SSD1306_DEACTIVATE_SCROLL);
};


/* Invert display */
void
SSD1306::invertDisplay(bool i)
const
{
  if (i)
    SSD1306::send_command(SSD1306_INVERTDISPLAY);
  else
    SSD1306::send_command(SSD1306_NORMALDISPLAY);
}



/* Turn on the display */
void
SSD1306::displayOn(void)
const
{
  send_command(SSD1306_DISPLAYON);
};


/* Turn off the display */
void
SSD1306::displayOff(void)
const
{
  clearDisplay();
  send_command(SSD1306_DISPLAYOFF);
};


/* Dim the display
 * dim(true): display is dimmed
 * dim(false): display is normal
 */
void
SSD1306::dim(bool dim)
const
{
  uint8_t c;

  if (dim)
    c = 0x00;  // Dimmed display
  else
  {
    // the range of contrast to too small to be really useful
    // it is useful to dim the display
    c = contrast;
  }
  SSD1306::send_command(SSD1306_SETCONTRAST, c);
}


/* Clear the display */
void
SSD1306::clearDisplay(void)
const
{
  memset(buffer1->data_ptr, 0, buffer1->data_size * sizeof(uint8_t));
  cursor_y = 0;
  cursor_x = 0;
};

/* Set font size */
void
SSD1306::setTextSize(uint8_t s)
const
{
  text_size = (s > 0) ? s : 1;
};

/* Activate a right handed scroll for rows start through stop
 * Hint, the display is 16 rows tall. To scroll the whole display, run:
 * startScrollRight(0x00, 0x0F)
 */
void
SSD1306::startScrollRight(uint8_t start, uint8_t stop)
const
{
  send_command(SSD1306_RIGHT_HORIZONTAL_SCROLL, 0x00); // 0x00 - dummy byte

  send_command(start);
  send_command(0X00);
  send_command(stop);
  send_command(0x00, 0xff); // Pair of dummy bytes

  send_command(SSD1306_ACTIVATE_SCROLL);
}

/* Activate a right handed scroll for rows start through stop
 * Hint, the display is 16 rows tall. To scroll the whole display, run:
 * startScrollLeft(0x00, 0x0F)
 */
void
SSD1306::startScrollLeft(uint8_t start, uint8_t stop)
const
{
  send_command(SSD1306_LEFT_HORIZONTAL_SCROLL, 0x00); // 0x00 - dummy byte

  send_command(start);
  send_command(0X00);
  send_command(stop);
  send_command(0x00, 0xff); // Pair of dummy bytes

  send_command(SSD1306_ACTIVATE_SCROLL);
}

/* Activate a diagonal scroll for rows start through stop
 * Hint, the display is 16 rows tall. To scroll the whole display, run:
 * startScrollDiagRight(0x00, 0x0F)
 */
void
SSD1306::startScrollDiagRight(uint8_t start, uint8_t stop)
const
{
  send_command(SSD1306_SET_VERTICAL_SCROLL_AREA, 0x00, height);

  send_command(SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL, 0x00); // 0x00 - dummy byte
  send_command(start);
  send_command(0x00); // Time interval 5 frames 0..7=[5,64,128,256,3,4,25,2]
  send_command(stop);
  send_command(0x01); // Vertical scrolling offset - one row

  send_command(SSD1306_ACTIVATE_SCROLL);
}

/* Activate a diagonal scroll for rows start through stop
 * Hint, the display is 16 rows tall. To scroll the whole display, run:
 * startScrollDiagLeft(0x00, 0x0F)
 */
void
SSD1306::startScrollDiagLeft(uint8_t start, uint8_t stop)
const
{
  send_command(SSD1306_SET_VERTICAL_SCROLL_AREA, 0x00, height);

  send_command(SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL, 0x00); // 0x00 - dummy byte

  send_command(start);
  send_command(0x00); // Time interval 5 frames 0..7=[5,64,128,256,3,4,25,2]
  send_command(stop);
  send_command(0x01); // Vertical scrolling offset - one row

  send_command(SSD1306_ACTIVATE_SCROLL);
}

/* Stop scroll */
void
SSD1306::stopScroll(void)
const
{
  send_command(SSD1306_DEACTIVATE_SCROLL);
};

void
SSD1306::drawString(char *str)
const
{
  int i, end;
  end = strlen(str);

  for (i = 0; i < end; i++)
  {
    unsigned char c = str[i];
    if (c == '\n')
    {
      cursor_y += text_size * 8;
      cursor_x = 0;
    }
    else if (c == '\r')
    {
      // skip em
    }
    else {
      drawChar(cursor_x, cursor_y, c, WHITE, text_size);
      cursor_x += text_size * 6;
      if (wrap && (cursor_x > (width - text_size * 6))) {
        cursor_y += text_size * 8;
        cursor_x = 0;
      }
    }
  }
};

// Draw a character
void
SSD1306::drawChar(int16_t x, int16_t y, unsigned char c, uint8_t color, uint8_t size)
const
{
  if ((x < width) &&  // Clip right
      (y < height) &&  // Clip bottom
      ((x + 6 * size - 1) >= 0) &&  // Clip left
      ((y + 8 * size - 1) >= 0))  // Clip top
  {
    uint8_t i;
    uint8_t j;
    for (i = 0; i < 6; i++) {
      uint8_t line;
      if (i == 5)
        line = 0x0;
      else
        line = pgm_read_byte(font + (c * 5) + i);
      for (j = 0; j < 8; j++)
      {
        if (line & 0x1)
        {
          if (size == 1)  // default size
          {
            drawPixel(x + i, y + j, color);
          }
          else
          {  // big size
            fillRect(x + (i * size),
                 y + (j * size), size,
                 size, color);
          };
        };
        line >>= 1;
      };
    };
  };
};

// the most basic function, set a single pixel
void
SSD1306::drawPixel(int16_t x, int16_t y, uint8_t color)
const
{
  if ((x >= 0) && (x < width) && (y >= 0) && (y < height)) {
    // check rotation, move pixel around if necessary
    switch(rotation) {
    case 0:
      break;
    case 1:
      swap_values(x, y);
      x = width - x - 1;
      break;
    case 2:
      x = width - x - 1;
      y = height - y - 1;
      break;
    case 3:
      swap_values(x, y);
      y = height - y - 1;
      break;
    }

    // x is which column
    switch (color) {
      case WHITE:   buffer1->data_ptr[x + (y / 8) * width] |=  (1 << (y & 7)); break;
      case BLACK:   buffer1->data_ptr[x + (y / 8) * width] &= ~(1 << (y & 7)); break;
      case INVERSE: buffer1->data_ptr[x + (y / 8) * width] ^=  (1 << (y & 7)); break;
    }
  }
};


bool
SSD1306::getPixel(int16_t x, int16_t y)
const
{
  if((x >= 0) && (x < width) && (y >= 0) && (y < height))
  {
    // Pixel is in-bounds. Rotate coordinates if needed.
    switch(rotation)
    {
      case 0:
        break;
      case 1:
        swap_values(x, y);
        x = width - x - 1;
        break;
      case 2:
        x = width  - x - 1;
        y = height - y - 1;
        break;
      case 3:
        swap_values(x, y);
        y = height - y - 1;
        break;
    }
    return (buffer1->data_ptr[x + (y / 8) * width] & (1 << (y & 7)));
  };
  return false; // Pixel out of bounds
};


void
SSD1306::drawFastHLineInternal(int16_t x, int16_t y, int16_t w, uint8_t color)
const
{
  // Do bounds/limit checks
  if (y < 0 || y >= height)
    return;

  // make sure we don't try to draw below 0
  if (x < 0) {
    w += x;
    x = 0;
  }
  // make sure we don't go off the edge of the display
  if ((x + w) > width)
    w = (width - x);

  // if our width is now negative, punt
  if (w <= 0)
    return;

  // set up the pointer for movement through the buffer
  uint8_t *pBuf = buffer1->data_ptr;
  // adjust the buffer pointer for the current row
  pBuf += ((y / 8) * width);
  // and offset x columns in
  pBuf += x;

  unsigned int mask = 1 << (y & 7);

  switch (color) {
  case WHITE:
    while (w--) {
      *pBuf++ |= mask;
    };
    break;
  case BLACK:
    mask = ~mask;
    while (w--) {
      *pBuf++ &= mask;
    };
    break;
  case INVERSE:
    while (w--) {
      *pBuf++ ^= mask;
    };
    break;
  }
};


void
SSD1306::drawFastVLineInternal(int16_t x, int16_t y, int16_t h, uint8_t color)
const
{
  // do nothing if we're off the left or right side of the screen
  if (x < 0 || x >= width)
    return;

  // make sure we don't try to draw below 0
  if (y < 0) {
    // y is negative, this will subtract enough from h to account
    // for y being 0
    h += y;
    y = 0;

  }
  // make sure we don't go past the height of the display
  if ((y + h) > height)
    h = (height - y);

  // if our height is now negative, punt
  if (h <= 0)
    return;

  // set up the pointer for fast movement through the buffer
  uint8_t *pBuf = buffer1->data_ptr;
  // adjust the buffer pointer for the current row
  pBuf += ((y / 8) * width);
  // and offset x columns in
  pBuf += x;

  // do the first partial byte, if necessary - this requires some
  // masking
  uint8_t mod = (y & 7);
  if (mod) {
    // mask off the high n bits we want to set
    mod = 8 - mod;

    // note - lookup table results in a nearly 10% performance
    // improvement in fill* functions
    // register unsigned int mask = ~(0xFF >> (mod));
    static uint8_t premask[8] =
        { 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };
    uint8_t mask = premask[mod];

    // adjust the mask if we're not going to reach the end of this
    // byte
    if (h < mod)
      mask &= (0XFF >> (mod - h));

    switch (color) {
    case WHITE:
      *pBuf |= mask;
      break;
    case BLACK:
      *pBuf &= ~mask;
      break;
    case INVERSE:
      *pBuf ^= mask;
      break;
    }

    // fast exit if we're done here!
    if (h < mod)
      return;

    h -= mod;

    pBuf += width;
  }
  // write solid bytes while we can - effectively doing 8 rows at a time
  if (h >= 8) {
    if (color == INVERSE) {  // separate copy of the code so we don't
      // impact performance of the black/white
      // write version with an extra comparison
      // per loop
      do {
        *pBuf = ~(*pBuf);

        // adjust the buffer forward 8 rows worth of data
        pBuf += width;

        // adjust h & y (there's got to be a faster way for me to
        // do this, but this should still help a fair bit for now)
        h -= 8;
      }
      while (h >= 8);
    } else {
      uint8_t val = (color == WHITE) ? 255 : 0;

      do {
        // write our value in
        *pBuf = val;

        // adjust the buffer forward 8 rows worth of data
        pBuf += width;

        // adjust h & y (there's got to be a faster way for me to
        // do this, but this should still help a fair bit for now)
        h -= 8;
      }
      while (h >= 8);
    }
  }
  // now do the final partial byte, if necessary
  if (h) {
    mod = h & 7;
    // this time we want to mask the low bits of the byte, vs the high 
    // bits we did above
    // register unsigned int mask = (1 << mod) - 1;
    // note - lookup table results in a nearly 10% performance
    // improvement in fill* functions
    static uint8_t postmask[8] =
        { 0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F };
    uint8_t mask = postmask[mod];

    switch (color) {
    case WHITE:
      *pBuf |= mask;
      break;
    case BLACK:
      *pBuf &= ~mask;
      break;
    case INVERSE:
      *pBuf ^= mask;
      break;
    }
  }
};


void
SSD1306::drawFastHLine(uint8_t x, uint8_t y, uint8_t w, uint8_t color)
const
{
  bool bSwap = false;

  switch (rotation) {
  case 0:
    // 0 degree rotation, do nothing
    break;
  case 1:
    // 90 degree rotation, swap x & y for rotation, then invert x
    bSwap = true;
    swap_values(x, y);
    x = width - x - 1;
    break;
  case 2:
    // 180 degree rotation, invert x and y - then shift y around for
    // height.
    x = width - x - 1;
    y = height - y - 1;
    x -= (w - 1);
    break;
  case 3:
    // 270 degree rotation, swap x & y for rotation, then invert y and 
    // adjust y for w (not to become h)
    bSwap = true;
    swap_values(x, y);
    y = height - y - 1;
    y -= (w - 1);
    break;
  }

  if (bSwap)
    drawFastVLineInternal(x, y, w, color);
  else
    drawFastHLineInternal(x, y, w, color);
};


void
SSD1306::drawFastVLine(uint8_t x, uint8_t y, uint8_t h, uint8_t color)
const
{
  bool bSwap = false;

  switch (rotation) {
  case 0:
    break;
  case 1:
    // 90 degree rotation, swap x & y for rotation, then invert x and
    // adjust x for h (now to become w)
    bSwap = true;
    swap_values(x, y);
    x = width - x - 1;
    x -= (h - 1);
    break;
  case 2:
    // 180 degree rotation, invert x and y - then shift y around for
    // height.
    x = width - x - 1;
    y = height - y - 1;
    y -= (h - 1);
    break;
  case 3:
    // 270 degree rotation, swap x & y for rotation, then invert y
    bSwap = true;
    swap_values(x, y);
    y = height - y - 1;
    break;
  }

  if (bSwap)
    SSD1306::drawFastHLineInternal(x, y, h, color);
  else
    SSD1306::drawFastVLineInternal(x, y, h, color);
}


/* Draw a rectangle by the color from (x, y), width=w, height=h */
void
SSD1306::drawRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color)
const
{
  // Bounds check
  if ((x >= width) || (y >= height))
    return;

  // Y bounds check
  if (y + h > height)
    h = height - y - 1;

  // X bounds check
  if (x + w > width)
    w = width - x - 1;

  switch (rotation) {
  case 0:
    break;
  case 1:
    swap_values(x, y);
    x = width - x - 1;
    break;
  case 2:
    x = width - x - 1;
    y = height - y - 1;
    break;
  case 3:
    swap_values(x, y);
    y = height - y - 1;
    break;
  }

  drawFastHLine(x    , y    , w, color);
  drawFastHLine(x    , y+h-1, w, color);
  drawFastVLine(x    , y    , h, color);
  drawFastVLine(x+w-1, y    , h, color);
}


/* Fill a rectangle with the color from (x, y), width=w, height=h */
void
SSD1306::fillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t color)
const
{
  // Bounds check
  if ((x >= width) || (y >= height))
    return;

  // Y bounds check
  if (y + h > height)
    h = height - y - 1;

  // X bounds check
  if (x + w > width)
    w = width - x - 1;

  switch (rotation) {
  case 0:
    break;
  case 1:
    swap_values(x, y);
    x = width - x - 1;
    break;
  case 2:
    x = width - x - 1;
    y = height - y - 1;
    break;
  case 3:
    swap_values(x, y);
    y = height - y - 1;
    break;
  }

  uint16_t i;
  for (i = 0; i < h; i++)
    drawFastHLine(x, y+i, w, color);
};


/*
   Redraw contents of display buffer.
   All changes made before will be displayed.
 */
void
SSD1306::display(void)
const
{
  /* Column start (0 = reset) and stop (127 = reset) addresses */
  send_command(SSD1306_COLUMNADDR, 0, width-1);
  /* Set Page start and stop addresses */
  //send_command(SSD1306_PAGEADDR, page_start, page_end);
  send_command(SSD1306_PAGEADDR, 0x00, 0xff); // 0xff from Adafruit source. Don't know why
  /* Send all data from data_reg[1]+buffer[buffer_size] */
  send_data_buffer();
};


/*
   Fill buffer of display by Adafruit logo data.
   For display the data run display()
 */
void
SSD1306::drawLogo(void)
const
{
  /* Copy actual size of logo data to the buffer for next redraw the screen */
  memcpy(buffer1->data_ptr, logo, buffer1->data_size);
};

/*
   Methods for Adafruit SSD1306 128x32 class
 */
#ifdef SSD1306_GPIO

SSD1306_128X32::SSD1306_128X32(gpio_pin_t pinClk, gpio_pin_t pinData, const char* gpiocDevice, uint8_t slaveAddress)
  :SSD1306::SSD1306(pinClk, pinData, gpiocDevice, 128, 32, slaveAddress)

#else

SSD1306_128X32::SSD1306_128X32(const char* iicDevice, uint8_t slaveAddress)
  :SSD1306::SSD1306(iicDevice, 128, 32, slaveAddress)

#endif
{
  contrast  = 0x7f;
  if (vccstate == SSD1306_EXTERNALVCC)
  {
    precharge = 0x22;
    charge_pump = 0x10;
  }
  else
  {
    precharge = 0xf1;
    charge_pump = 0x14;
  }

  com_pins = 0x02;
};

SSD1306_128X32::~SSD1306_128X32()
{
  SSD1306::~SSD1306();
};


/*
   Methods for Adafruit SSD1306 128x64 class
 */
#ifdef SSD1306_GPIO

SSD1306_128X64::SSD1306_128X64(gpio_pin_t pinClk, gpio_pin_t pinData, const char* gpiocDevice, uint8_t slaveAddress)
  :SSD1306::SSD1306(pinClk, pinData, gpiocDevice, 128, 64, slaveAddress)

#else

SSD1306_128X64::SSD1306_128X64(const char* iicDevice, uint8_t slaveAddress)
  :SSD1306::SSD1306(iicDevice, 128, 64, slaveAddress)

#endif
{
  if (vccstate == SSD1306_EXTERNALVCC)
  {
    contrast = 0x9f;
    precharge = 0x22;
    charge_pump = 0x10;
  }
  else
  {
    contrast  = 0xcf;
    precharge = 0xf1;
    charge_pump = 0x14;
  }

  com_pins = 0x12;
};

SSD1306_128X64::~SSD1306_128X64()
{
  SSD1306::~SSD1306();
};


/*
   Methods for Adafruit SSD1306 96x16 class
 */
#ifdef SSD1306_GPIO

SSD1306_96X16::SSD1306_96X16(gpio_pin_t pinClk, gpio_pin_t pinData, const char* gpiocDevice, uint8_t slaveAddress)
  :SSD1306::SSD1306(pinClk, pinData, gpiocDevice, 96, 16, slaveAddress)

#else

SSD1306_96X16::SSD1306_96X16(const char* iicDevice, uint8_t slaveAddress)
  :SSD1306::SSD1306(iicDevice, 96, 16, slaveAddress)

#endif
{
  if (vccstate == SSD1306_EXTERNALVCC)
  {
    contrast = 0x10;
    precharge = 0x22;
    charge_pump = 0x10;
  }
  else
  {
    contrast  = 0xaf;
    precharge = 0xf1;
    charge_pump = 0x14;
  }

  com_pins = 0x02;
};

SSD1306_96X16::~SSD1306_96X16()
{
  SSD1306::~SSD1306();
};
